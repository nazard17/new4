$(document).ready(function(){
    window.onload=function(){
    init();
    };
    function init() {
        autoPlayBar();
        autoPlay();
        paginClick();
            swipe();
            swipeBar();
            binds();
        };
        new WOW().init();
        $('.grid_blog_list').masonry({
            itemSelector: '.grid_blog_item',
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer',
            percentPosition: true
    });
            $('#stars li').on('mouseover', function(){
              var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
              // Now highlight all the stars that's not after the current hovered star
              $(this).parent().children('li.star').each(function(e){
                if (e < onStar) {
                  $(this).addClass('hover');
                }
                else {
                  $(this).removeClass('hover');
                }
              });

            }).on('mouseout', function(){
              $(this).parent().children('li.star').each(function(e){
                $(this).removeClass('hover');
              });
            });
            $('#stars li').on('click', function(){
              var onStar = parseInt($(this).data('value'), 10); // The star currently selected
              var stars = $(this).parent().children('li.star');
              for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('selected');
              }
              for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('selected');
              }
            });
            function binds() {
                   $('.button-next').click(function () {
                     slideNext();
                     autoPlay();
                   });
                   $('.button-prev').click(function () {
                      slidePrev();
                     autoPlay();
                   });
                   $('.bar_next').click(function () {
                       slideNextBar();
                      autoPlayBar();
                   });
                   $('.bar_prev').click(function () {
                       slidePrevBar();
                     autoPlayBar();
                   });
                 };

    function slideBar(index) {
        $('.bar-slide.active_bar').removeClass('active_bar');
        $('.bar-slide.active_bar').fadeOut(100);
        $('.bar-slide').eq(index).addClass('active_bar').fadeIn(1000);
        $('.bar-slide').first().eq().addClass('active_bar');
    };
    function slideNextBar() {
         var index = $('.bar-slide.active_bar').index() + 1;
         var last = $('.bar-slide').length;
         if(index == last){
             index = 0;
         }
         slideBar(index);
    };

     function slidePrevBar() {
         var index = $('.bar-slide.active_bar').index() - 1;
         slideBar(index);
     };

    function slideTo(index) {
        $('.mainpage_slide_content.active').fadeOut(100);
        $('.mainpage_slide_content.active').removeClass('active');
        $('.mainpage_slide_content').eq(index).addClass('active').fadeIn(1000);
        $('.mainpage_slide_content').first().eq().addClass('active');


       $('.pugins.current_pugin').removeClass('current_pugin');
       $('.pugins').eq(index).addClass('current_pugin');
   };
           function slideNext() {
            var index = $('.mainpage_slide_content.active').index() + 1;
            var last = $('.mainpage_slide_content').length;
             if (index == last){
                 index = 0;
             }
            slideTo(index);
        };
        function slidePrev() {
            var index = $('.mainpage_slide_content.active').index() - 1;
            slideTo(index);
        };
           function paginClick() {
            $('.pugins').click(function () {
                var index = $(this).index();
                var currentSlide = $('.mainpage_slide_content.active');
                currentSlide.fadeOut(500)
                .removeClass('active');
                slideTo(index);
                autoPlay();
            });
        }

// MAIN SLIDDER SWIPE
       function swipe() {
        jQuery('.slider_content').swipe({
            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                if (direction == 'up') {
                     slidePrev();
                 }
                 if (direction == 'down') {
                     slideNext();
                 }
            }

        });
        autoPlay();
    };

//SIDE_BAR SWIPE
        function swipeBar() {
         jQuery('.side_bar_slider_content').swipe({
             swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                 if (direction == 'left') {
                      slidePrevBar();
                  }
                  if (direction == 'right') {
                      slideNextBar();
                  }
             }

         });
         autoPlayBar();
        };

// AUTO PLAY FOR MAIN SLIDE
        var autoPlayDuration = 5000;
        var autoPlayInterval;
        function autoPlay() {
        clearInterval(autoPlayInterval);
            autoPlayInterval =  setInterval(function () {
            slideNext();
        }, autoPlayDuration)
    };

// AUTO PLAY FOR SIDE_Bar
    var autoPlayDurationBar = 3000;
    var autoPlayIntervalBar;
         function autoPlayBar() {
             clearInterval(autoPlayIntervalBar);
                 autoPlayIntervalBar =  setInterval(function () {
                 slideNextBar();
             }, autoPlayDurationBar)
         };

//Toggle-Menu
        var ddd = $('.overlay');
        function openNav() {
            ddd.show();

        };
            $('.nav-menu').click(function () {
                console.log("hi");
                openNav();
            });
        function closeNav() {
            ddd.hide()
        };

        $('.closebtn').click(function () {
            closeNav();
        console.log('bye');
        });


});
